<?php

namespace Rlp\relatedBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('RlprelatedBundle:Default:index.html.twig', array('name' => $name));
    }
}
