<?php

namespace Rlp\relatedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Particulier
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rlp\relatedBundle\Entity\ParticulierRepository")
 */
class Particulier
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="listEnCours", type="object")
     */
    private $listEnCours;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="listEffectues", type="object")
     */
    private $listEffectues;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set listEnCours
     *
     * @param \stdClass $listEnCours
     * @return Particulier
     */
    public function setListEnCours($listEnCours)
    {
        $this->listEnCours = $listEnCours;
    
        return $this;
    }

    /**
     * Get listEnCours
     *
     * @return \stdClass 
     */
    public function getListEnCours()
    {
        return $this->listEnCours;
    }

    /**
     * Set listEffectues
     *
     * @param \stdClass $listEffectues
     * @return Particulier
     */
    public function setListEffectues($listEffectues)
    {
        $this->listEffectues = $listEffectues;
    
        return $this;
    }

    /**
     * Get listEffectues
     *
     * @return \stdClass 
     */
    public function getListEffectues()
    {
        return $this->listEffectues;
    }
}
