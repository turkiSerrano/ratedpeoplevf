<?php

namespace Rlp\relatedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Professionnel
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rlp\relatedBundle\Entity\ProfessionnelRepository")
 */
class Professionnel extends Utilisateur
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numSiret", type="string", length=255)
     */
    private $numSiret;

    /**
     * @var integer
     *
     * @ORM\Column(name="rayonAction", type="smallint")
     */
    private $rayonAction;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numSiret
     *
     * @param string $numSiret
     * @return Professionnel
     */
    public function setNumSiret($numSiret)
    {
        $this->numSiret = $numSiret;
    
        return $this;
    }

    /**
     * Get numSiret
     *
     * @return string 
     */
    public function getNumSiret()
    {
        return $this->numSiret;
    }

    /**
     * Set rayonAction
     *
     * @param integer $rayonAction
     * @return Professionnel
     */
    public function setRayonAction($rayonAction)
    {
        $this->rayonAction = $rayonAction;
    
        return $this;
    }

    /**
     * Get rayonAction
     *
     * @return integer 
     */
    public function getRayonAction()
    {
        return $this->rayonAction;
    }
}
